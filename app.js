var express = require("express");
var bodyParser = require("body-parser");
const logger = require('morgan');



var app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//Request logger middleware
var redis = require('redis');
var client = redis.createClient();
client.on('connect', function() {
  console.log('Redis client connected');
});
client.on('error', function (err) {
  console.log('Something went wrong ' + err);
});

app.use(function (req, res, next) {
    var url = req.url;

    client.get(url, function (error, result) {
        if (error) {
            console.log(error);
            throw error;
        }

        if(result === null){
            result = 1; //starts from 1
        }
        else {
            result = parseInt(result) + 1;
        }
        client.set(url, result, redis.print);
        console.log(url + ' : ' + result);
        
    });
    next();
  })

var routes = require("./routes");
routes(app,client);

var server = app.listen(3000, function () {
    console.log("app running on port.", server.address().port);
});
