'use strict';
var faker = require('faker');

module.exports = {
  up: (queryInterface, Sequelize) => {

    function restaurentGen(){

      function menuGen(){
        return {
          "title": faker.commerce.productName(),
          "price": faker.commerce.price(),
          "ratings": faker.random.number({
            'min': 0,
            'max': 5
          })
        }
      }

      return {
        data: JSON.stringify(
          {
            name: faker.company.companyName(),
            menu: [menuGen(),menuGen(),menuGen(),menuGen(),menuGen()],
            latlong: {
              lat: faker.random.number({
                'min': 0,
                'max': 90
              }),
              long: faker.random.number({
                'min': 80,
                'max': 95
              })
            }
          }
        )
        ,
        createdAt: faker.date.recent(), 
        updatedAt: faker.date.recent(),
      }
    }

    var dummyRestaurants = [];
    for(var i = 0; i < 30; i++){
      dummyRestaurants.push(restaurentGen());
    }

    return queryInterface.bulkInsert('Restaurants', dummyRestaurants, {});
    
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Restaurants', null, {});
  }
};
