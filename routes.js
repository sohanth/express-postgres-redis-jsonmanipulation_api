const restaurantController = require('./controllers/restaurantController')
const { check } = require('express-validator/check')
var async = require('async');

var baseRouter = function (app, client) {

  app.get('/api', (req, res) => res.status(200).send({
    message: 'Welcome to the API!',
  }));


  app.get('/api/restaurants', restaurantController.list);
  app.post('/api/restaurants', restaurantController.create);
  app.get('/api/restaurants/:id([0-9]+)', restaurantController.detail);
  app.get('/api/restaurants/:id([0-9]+)/menu', restaurantController.getMenu);

  app.get('/api/restaurants/filter/:name(\\w+)', restaurantController.filter);
  app.get('/api/menu-list/', restaurantController.menuFilter);

  app.put('/api/restaurants/:id([0-9]+)/latlong',
    [check('lat').isDecimal(), check('long').isDecimal()], restaurantController.updateLatLong);

  app.put('/api/restaurants/:id([0-9]+)/changeName',
    [check('name').isString()], restaurantController.changeName);
  app.put('/api/restaurants/:id([0-9]+)/addMenu',
    [check('title').isString(), check('price').isDecimal(), check('rating').isDecimal()], restaurantController.addMenu);

  app.get('/api/usage-stats', function (req, res) {
    client.keys('*', function (err, keys) {
      if (err) return console.log(err);
      if (keys) {
        async.map(keys, function (key, cb) {
          client.get(key, function (error, value) {
            if (error) return cb(error);
            var endpoint = {};
            endpoint['api'] = key;
            endpoint['hits'] = value;
            cb(null, endpoint);
          });
        }, function (error, results) {
          if (error) return console.log(error);
          res.json({
            stats: results
          });
        });
      }
    });
  })

}

module.exports = baseRouter;