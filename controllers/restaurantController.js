const Restaurant = require('../models').Restaurant;
const Sequelize = require('sequelize');
const db = require('../models');
var SqlString = require('sequelize/lib/sql-string');
const { validationResult } = require('express-validator/check');


function verifyValidation(req,res){
    const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
        }
}

module.exports = {
    list(req, res) {
        return Restaurant
            .all()
            .then(rsList => res.status(200).send(rsList))
            .catch(error => res.status(400).send(error));
    },
    create(req, res) {
        var data = JSON.parse(req.body.data);

        //Manual Json validation just for trying it out
        var errors = [];
        if(!data.hasOwnProperty('name')){
            errors.push("Name not given!");
        }

        if(!data.hasOwnProperty('menu')){
            errors.push("Menu not given!");

            if(Array.isArray(data.menu)){
                errors.push("Menu is not an array!");
            }
        }

        if(errors.length === 0){
            return Restaurant.create({
                data: data
            })
            .then(rst => res.status(201).send(rst))
            .catch(error => res.status(400).send(error));
        }
        else {
            res.status(400).send(errors);
        }

        
    },
    detail(req, res) {
        return Restaurant.findById(req.params.id)
            .then(function (rstObj) {
                if (rstObj === null) {
                    res.status(404).send("Not found");
                } else {
                    res.status(200).send(rstObj);
                }
            })
            .catch(error => res.status(400).send(error));
    },
    filter(req, res) {
        var filterName = req.params.name;

        const Op = Sequelize.Op;
        return Restaurant.findAll({
                where: {
                    data: {
                        name: {
                            [Op.like]: "%" + filterName + "%" //Does not need sanitazation apparently
                        }
                    }
                }
            })
            .then(function (rstArr) {
                res.status(200).send(rstArr);
            })
            .catch(error => res.status(400).send(error));
    },
    getMenu(req, res) {
        var id = req.params.id;
        db.sequelize.query('SELECT data -> \'menu\' as menu FROM "Restaurants" WHERE id = :pk', {
                replacements: {
                    pk: id
                },
                type: db.sequelize.QueryTypes.SELECT
            }).then(function (menus) {
                function first(p) {
                    for (var i in p) return p[i];
                }

                menus = first(menus).menu;

                res.status(200).send(menus);
            })
            .catch(error => res.status(400).send(error));
    },


    menuFilter(req, res) {

        var filter = req.query.filter;
        var filterVal = req.query.value;

        var query;

        switch (filter) {
            case 'minRating':
                var minRating = filterVal;
                query = db.sequelize.query(`SELECT menuItems FROM "Restaurants" r,jsonb_array_elements(r.data->\'menu\') menuItems WHERE CAST ( menuItems->>\'ratings\' AS DECIMAL) >= :minRating`, {
                    replacements: {
                        minRating: minRating
                    },
                    type: db.sequelize.QueryTypes.SELECT
                });
                break;
            case 'foodContains':
                var foodContains = '%' + filterVal + '%';
                query = db.sequelize.query(`SELECT menuItems FROM "Restaurants" r,jsonb_array_elements(r.data->\'menu\') menuItems WHERE LOWER(menuItems->>\'title\') LIKE LOWER(:foodContains)`, {
                    replacements: {
                        foodContains: foodContains
                    },
                    type: db.sequelize.QueryTypes.SELECT
                });
                break;
        }

        query.then(function (menus) {
                res.status(200).send(menus);
            })
            .catch(error => res.status(400).send(error));
    },

    updateLatLong(req, res) {
        verifyValidation(req,res);

        var id = req.params.id;
        var lat = req.body.lat;
        var long = req.body.long;

        var jsonObj = {lat: parseFloat(lat), long: parseFloat(long)};
        
        db.sequelize.query(`update "Restaurants" set data = jsonb_set(data,'{latlong}', :json) where id= :pk`, {
                replacements: {
                    json: JSON.stringify(jsonObj),
                    pk: id
                },
                type: db.sequelize.QueryTypes.UPDATE
            }).then(function () {
                res.status(200).send({success: "true"});
            })
            .catch(error => res.status(400).send(error));
    },
    changeName(req, res) {

        verifyValidation(req,res);

        var id = req.params.id;
        var name = JSON.stringify(req.body.name);
        
        db.sequelize.query(`update "Restaurants" set data = jsonb_set(data,'{name}', :name) where id= :pk`, {
                replacements: {
                    name: name,
                    pk: id
                },
                type: db.sequelize.QueryTypes.UPDATE
            }).then(function () {
                res.status(200).send({success: "true"});
            })
            .catch(error => res.status(400).send(error));
    },
    addMenu(req, res) {
        verifyValidation(req,res);


        var id = req.params.id;
        var title = req.body.title;
        var price = parseFloat(req.body.price);
        var rating = parseFloat(req.body.rating);

        var menuObj = {
            title : title,
            price : price,
            rating : rating
        };
        
        db.sequelize.query(`update "Restaurants" set data = jsonb_set(data,'{menu}', data -> 'menu' || :newMenuJson, true) where id= :pk`, {
                replacements: {
                    newMenuJson: JSON.stringify(menuObj),
                    pk: id
                },
                type: db.sequelize.QueryTypes.UPDATE
            }).then(function () {
                res.status(200).send({success: "true"});
            })
            .catch(error => res.status(400).send(error));
    },

};
